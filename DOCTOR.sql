CREATE TABLE DRUG (
    drug_id INT PRIMARY KEY AUTO_INCREMENT,
    drug_name VARCHAR(255)
);

CREATE TABLE SECRETARY (
    secretary_id INT PRIMARY KEY AUTO_INCREMENT,
    secretary_name VARCHAR(255)
);

CREATE TABLE PATIENT (
    patient_id INT PRIMARY KEY AUTO_INCREMENT,
    patient_name VARCHAR(255),
    patient_dob DATE,
    patient_address VARCHAR(255)
);

CREATE TABLE PRESCRIPTION (
    prescription_id INT PRIMARY KEY AUTO_INCREMENT,
    prescription_date DATE,
    dosage VARCHAR(255),
);

CREATE TABLE DOCTOR (
    doctor_id INT PRIMARY KEY AUTO_INCREMENT,
    doctor_name VARCHAR(255),
    patient_id INT,
    prescription_id INT,
    drug_id INT,
    secretary_id INT,
    FOREIGN KEY (drug_id) REFERENCES DRUG(drug_id),
    FOREIGN KEY (prescription_id) REFERENCES PRESCRIPTION(prescription_id),
    FOREIGN KEY (patient_id) REFERENCES PATIENT(patient_id),
    FOREIGN KEY (secretary_id) REFERENCES SECRETARY(secretary_id),
);