CREATE TABLE Manager (
  Manager_id INT PRIMARY KEY,
  Manager_name VARCHAR(255),
  Manager_location VARCHAR(255)
);

CREATE TABLE Contract (
  Contract_id INT PRIMARY KEY,
  Estimated_cost DECIMAL(10, 2),
  Completion_date DATE
);

CREATE TABLE Staff (
  Staff_id INT PRIMARY KEY,
  Staff_name VARCHAR(255),
  Staff_location VARCHAR(255)
);

CREATE TABLE Client (
  Client_ID INT PRIMARY KEY AUTO_INCREMENT,
  Client_Name VARCHAR(255),
  Client_Location VARCHAR(255),
  Manager_id INT,
  Contract_id INT,
  Staff_id INT,
  FOREIGN KEY (Manager_id) REFERENCES Manager(Manager_id),
  FOREIGN KEY (Contract_id) REFERENCES Contract(Contract_id),
  FOREIGN KEY (Staff_id) REFERENCES Staff(Staff_id)
);
