CREATE TABLE DRUG (
    DrugID INT PRIMARY KEY AUTO_INCREMENT,
    DrugName VARCHAR(255)
);

CREATE TABLE DOCTOR (
    DoctorID INT PRIMARY KEY AUTO_INCREMENT,
    DoctorName VARCHAR(255)
);

CREATE TABLE SECRETARY (
    SecretaryID INT PRIMARY KEY AUTO_INCREMENT,
    SecretaryName VARCHAR(255)
);

CREATE TABLE PRESCRIPTION (
    PrescriptionID INT PRIMARY KEY AUTO_INCREMENT,
    DescriptionDate DATE,
    Dosage VARCHAR(255),    
);

CREATE TABLE PATIENT (
    PatientID INT PRIMARY KEY AUTO_INCREMENT,
    PatientName VARCHAR(255),
    PatientDOB DATE,
    PatientAddress VARCHAR(255),
    DoctorID INT,
    SecretaryID INT,
    DrugID INT,
    PrescriptionID INT,
    FOREIGN KEY (PrescriptionID) REFERENCES PRESCRIPTION(PrescriptionID),
    FOREIGN KEY (DrugID) REFERENCES DRUG(DrugID),
    FOREIGN KEY (DoctorID) REFERENCES DOCTOR(DoctorID),
    FOREIGN KEY (SecretaryID) REFERENCES SECRETARY(SecretaryID)
);