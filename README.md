### Database Design Drills

Normalize Each Of The Following Tables

For each task, create a separate `.sql` file with the create statements for the tables in a GitLab repository and submit the repo.

**DO NOT SUBMIT mysqldump files**. You have to create the tables in the SQL files and submit them directly.

#### Task 1: BRANCH

- Branch#
- Branch_Addr
- ISBN
- Title
- Author
- Publisher
- Num_copies

#### Task 2: CLIENT

- Client#
- Name
- Location
- Manager#
- Manager_name
- Manager_location
- Contract#
- - Estimated_cost
- Completion_date
- Staff#
- Staff_name
- Staff_location

#### Task 3: PATIENT

- Patient#
- Name
- DOB
- Address
- Prescription#
- Drug
- Date
- Dosage
- Doctor
- Secretary

#### Task 4 :DOCTOR

- Doctor#
- DoctorName
- Secretary
- Patient#
- PatientName
- PatientDOB
- PatientAddress
- Prescription#
- Drug
- Date
- Dosage